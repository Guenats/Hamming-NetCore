﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hamming_NetCore.Class
{
    public class Matrix
    {
        public static void AfficheMatrice(int[,] tab)
        {
            int rowLength = tab.GetLength(0);
            int colLength = tab.GetLength(1);
            int test = tab.Rank;

            for (int i = 0; i < rowLength; i++)
            {
                for (int j = 0; j < colLength; j++)
                {
                    Console.Write(string.Format("{0} ", tab[i, j]));
                }
                Console.Write(Environment.NewLine);
            }
            Console.ReadLine();
        }

        public static int CompareOccurenceString(string item1, string item2)
        {
            int i = 0;
            int result = 0;
            foreach (char charItem1 in item1)
            {
                if (charItem1 == item2[i])
                {
                    result += 1;
                }
                i++;
            }
            return result;
        }

        public static List<Line> AfficheHammingDistance(int[,] tab)
        {
            List<Croissement> listCroissement = new List<Croissement>();
            int rowLength = tab.GetLength(0);
            int colLength = tab.GetLength(1);
            int l = 0;
            int k = 1; ;
            List<string> listString= new List<string>() ;
            List<Line> listLine = new List<Line>();
            string line="";

            int[,] tabHamming = new int[10, 10];

            for (int i = 0; i < rowLength; i++)
            {
                for (int j = 0; j < colLength; j++)
                {
                    line += tab[i, j];
                }
                listString.Add(line);
                line = "";
            }

            foreach (string item in listString)
            {
                for (k = 0; k < listString.Count; k++)
                {
                    if (item != listString[k])
                    {
                        Croissement croissement = new Croissement(item, listString[k], CompareOccurenceString(item, listString[k]));
                        listCroissement.Add(croissement);
                    }
                }
                Line newLine = new Line(item, listCroissement.Average(croissement => croissement.DistanceHamming));
                listLine.Add(newLine);
                listCroissement.Clear();
                l++;
            }
            return listLine;
        }



        public static void DiviseInCluster(List<Line> listLine)
        {
            int n = 1;
            List<Cluster> listCluster = new List<Cluster>();
            foreach (var item in listLine)
            {
                if (listCluster.Count == 0)
                {
                    listCluster.Add(new Cluster(item.MoyenneDistanceHamming, new List<Line>() { item }));
                }
                else if (listCluster.Exists(itemLine => itemLine.Pourcentage == item.MoyenneDistanceHamming))
                {
                    Cluster cluster = listCluster.Single(itemLine => itemLine.Pourcentage == item.MoyenneDistanceHamming);
                    cluster.ListLine.Add(item);
                }
                else
                {
                    listCluster.Add(new Cluster(item.MoyenneDistanceHamming, new List<Line>() { item }));
                }
            }
                foreach (var line in listCluster)
                {
                    Console.WriteLine("Cluster "+n+" ;"+" Ecart "+ line.Pourcentage);
                    foreach (var itemLine in line.ListLine)
                    {
                        Console.WriteLine(itemLine.LineMatrix);
                    }
                n++;
                }

            }
        }
    }
